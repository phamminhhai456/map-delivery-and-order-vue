async function login() {
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    try {
        const response = await fetch('https://map-delivery-and-order-express.onrender.com/api/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json',
            },
            body: JSON.stringify({ username, password}),
        })

        const result = await response.json();
        

        if (result.success) {           
                window.location.href = `/map.html?userId=${result.User._id}`;            
        } else {
            document.getElementById('result').innerHTML = `<p style="color: red;">${result.message}</p>`;
            console.log(result.message);
        }
    } catch (error) {
        console.error('Error:', error);
        alert('An error occurred');
    }
}


