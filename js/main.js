// [032403Tree] CONST variables
const env = "prod"; // dev
const host_wp =
  env === "prod"
    ? "https://staging.foodiemenu.co/wp-json/tv-menu-api"
    : "http://localhost/wordpress/wp-json/tv-menu-api";
const accessToken =
  "pk.eyJ1IjoiYnJvbHk5OTk4IiwiYSI6ImNscnI5cG14cTAxZ28ydnBjcGx3a2xkaWEifQ.GJLQq76LqceD19Q6abBjrQ";
const geocoding_url = "https://api.mapbox.com/geocoding/v5/mapbox.places/";
const host_node =
  env === "prod"
    ? "https://tree-test-tracking.onrender.com/api"
    : "http://localhost:3000/api";
const host_socket = env === "prod" ? "https://tree-test-tracking.onrender.com" : "http://localhost:3000";
const mapbox_style = "mapbox://styles/mapbox/streets-v11";
const DRAW_ROUTE_DELAY = 3000;
const DRAW_MARKER_DELAY = 1000;

var ap = new Vue({
  el: "#app",
  data: {
    showAssignPopup: "",
    selectedDriver: {},
    selectedStatusOrder: "completed",
    deliveryNotes: "",
    currentAddress: "",
    map: [],
    showRoutes: false,
    drivers: [],
    orders: [],
    driver: [],
    admin: [],
    ordersDriver: [],
    markers: [],
    role: "",
    socket: null,
    coordinates: [],
    currentLocation: null,
    isDrawingRoute: false,
    isDrawMarker: false,
    currentMarker: null,
    currentLine: null,
  },
  methods: {
    async updateLocDriver(longitude, latitude){
      try {
        const response = await fetch(
          `${host_node}/driver/updateLocId/${this.admin?._id}`,
          {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
              locID: [longitude, latitude],
            }),
          }
        );
        const data = await response.json();
        if (data.success) {
          this.driver = data.driver;
          return true;
        }
        return false;
      } catch (error) {
        alert("Something went wrong")
        console.log(error);
      }
    },
    //loadmap
    loadmap: async function () {
      var self = this; // Lưu trữ ngữ cảnh của this
      navigator.geolocation.getCurrentPosition(async (position) => {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        if (!mapboxgl.supported()) {
          alert("Your browser does not support Mapbox GL");
        } else {
          const response = await this.updateLocDriver(longitude, latitude);
          if (response) {
            self.currentLocation = [longitude, latitude];
            mapboxgl.accessToken = accessToken;
            self.map = new mapboxgl.Map({
              container: "map",
              style: mapbox_style,
              center: [longitude, latitude],
              zoom: 14,
            });

            self.showIconDriver(this.driver);
            self.map.addControl(
              new mapboxgl.NavigationControl(),
              "bottom-right"
            );

            // marker = new mapboxgl.Marker()
            //   .setLngLat([longitude, latitude])
            //   .addTo(map);
          } else {
            alert("Error occurred when loading your location");
          }
        }
        // Initialize Socket.IO
        self.socket = io(host_socket);

        // Listen for location updates from the server
        self.socket.on("locationUpdate", (data) => {
          const latitude = data.latitude;
          const longitude = data.longitude;
          currentLocation = [longitude, latitude];
          coordinates.push([longitude, latitude]);
          if (marker) {
            marker.setLngLat([longitude, latitude]);
          }

          // Create a new <p> element to display location information
          // const locationInfo = document.createElement("p");
          // locationInfo.textContent = `Latitude: ${latitude}, Longitude: ${longitude}`;

          // Append location information to the "result" element
          // result.appendChild(locationInfo);
        });

        // socket.on("routeUpdate", (data) => {
        //   console.log("route duration: " + data);

        //   // Create a new <p> element to display location information
        //   const locationInfo = document.createElement("p");
        //   locationInfo.textContent = `Duration: ${data.totalDuration}`;

        //   // Append location information to the "result" element
        //   duration.appendChild(locationInfo);
        // });
      });
    },
    //Check user role when log in
    checkUserRole: async function () {
      // check user route
      const userId = this.getUserIdFromUrl();
      try {
        const response = await fetch(`${host_node}/user/${userId}`);
        const data = await response.json();
        if (data.success) {
          this.role = data.User.role;
          if (data.User.role === "driver") {
            this.driver = data.User;
          }
          this.admin = data.User;
        } else {
          console.error("Failed to fetch drivers:", data.message);
        }
      } catch (error) {
        console.error("Error:", error);
      }
    },

    // [102403Tree]
     trackingLocation(){
      var self = this;
      navigator.geolocation.watchPosition(
        async (data) => {
          const latitude = data.coords.latitude;
          const longitude = data.coords.longitude;
          self.currentLocation = [longitude, latitude];
          console.log(data);
          self.coordinates.push([longitude, latitude]);
          // console.log("list: " + coordinates);

          // Emit the location data to the server
          self.socket.emit("locationUpdate", {
            latitude: latitude,
            longitude: longitude,
          });


          

          if (!self.isDrawMarker) {
            self.isDrawMarker = true;

            setTimeout(async () => {
              // Update the marker position on the map
              // if (marker) {
              //   marker.setLngLat([longitude, latitude]);
              // }
              var targetMarkerIndex = self.findMarkerByCoordinate(self.currentLocation, self);
              if (targetMarkerIndex !== -1) {
                self.markers[targetMarkerIndex].remove();
                self.markers.splice(targetMarkerIndex, 1);
              }
              self.showIconDriver(self.driver);
              await this.updateLocDriver(longitude, latitude);
              //  Create a new <p> element to display location information
              // const locationInfo = document.createElement("p");
              // locationInfo.textContent = `Latitude: ${latitude}, Longitude: ${longitude}`;

              // Append location information to the "result" element
              // result.appendChild(locationInfo);

              self.isDrawMarker = false;
            }, DRAW_MARKER_DELAY);
          }
        },
        (error) => console.log(error),
        {
          enableHighAccuracy: true,
        }
      );
    },
    // hide/show instructions
    toggleRoutes: function () {
      this.showRoutes = !this.showRoutes;
      var instructions = document.getElementById("instructions");
      var sidebar = document.getElementById("mySidebar");
      if (this.showRoutes) {
        instructions.classList.remove("hidden");
        sidebar.classList.add("hidden");
      } else {
        instructions.classList.add("hidden");
        sidebar.classList.remove("hidden");
      }
    },

    // hide/show sidebar
    toggleSidebar: function () {
      var sidebar = document.getElementById("mySidebar");
      if (sidebar.className.includes("hidden")) {
        sidebar.classList.remove("hidden");
      } else {
        sidebar.classList.add("hidden");
      }
    },

    //get user id from url
    getUserIdFromUrl: function () {
      let currentUrl = window.location.href;

      let url = new URL(currentUrl);

      let searchParams = new URLSearchParams(url.search);

      let driverId = searchParams.get("userId");

      return driverId;
    },

    //Get list driver
    async getDrivers() {
      try {
        const response = await fetch(`${host_node}/driver/getAllDriver`);
        const data = await response.json();
        if (data.success) {
          this.drivers = data.drivers.map((driver) => ({
            id: driver._id,
            username: driver.username,
            phoneNo: driver.phoneNo,
            locID: driver.locID,
          }));
        } else {
          console.error("Failed to fetch drivers:", data.message);
        }
      } catch (error) {
        console.error("Error:", error);
      }
    },

    // [032403Tree] convert address to coordinates
    async convertAddressToCoordinate(address) {
      let urlGeocoding =
        geocoding_url + address + ".json?access_token=" + accessToken;
      try {
        const response = await fetch(urlGeocoding);
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }

        const data = await response.json();
        return data;
      } catch (error) {
        console.error("There was a problem with your fetch operation:", error);
      }
    },

    async getDriverById(driverId) {
      try {
        const res = await fetch(
          `${host_node}/driver/getDriverById/${driverId}`
        );
        const data = await res.json();

        if (data.success) {
          this.driver = data.Driver;
        }
      } catch (err) {
        alert("Something went wrong !!!");
      }
    },

    async getDriverByPhone(driverPhone) {
      try {
        const res = await fetch(
          `${host_node}/driver/getDriverByPhone/${driverPhone}`
        );
        const data = await res.json();

        if (data.success) {
          return data.Driver.username;
        } else {
          return "";
        }
      } catch (err) {
        alert("Something went wrong !!!");
      }
    },

    // [032403Tree]
    async convertDataOrders(data) {
      return await Promise.all(
        data.map(async (order) => {
          const addressConverted = await this.convertAddressToCoordinate(
            order.shipAddr
          );
          const locID = addressConverted?.features[0]?.geometry?.coordinates;
          let driverName = "";
          if (order.driverPhone) {
            driverName = await this.getDriverByPhone(order.driverPhone);
          }
          return {
            id: order.id,
            cusName: order.customer,
            cusPhone: order.customerPhone,
            driverPhone: order.driverPhone,
            locID: locID,
            shipAddr: order.shipAddr,
            driverName: driverName,
          };
        })
      );
    },
    //get list order
    async getOrders() {
      //   try {
      //     const response = await fetch(
      //       "${host_node}/order/getAll"
      //     );
      //     const data = await response.json();
      //     if (data.success) {
      //       this.orders = data.orders.map((order) => ({
      //         id: order._id,
      //         cusName: order.cusName,
      //         cusPhone: order.cusPhone,
      //         driverPhone: order.driverPhone,
      //         locID: order.locID,
      //         shipAddr: order.shipAddr,
      //       }));
      //     } else {
      //       console.error("Failed to fetch orders:", data.message);
      //     }
      //   } catch (error) {
      //     console.error("Error:", error);
      //   }
      // [032403Tree] fetch orders from db wp
      try {
        const vendorId = 1010;
        const statusOrder = "processing";
        let page = 1;
        const limit = 5;
        const response = await fetch(
          `${host_wp}/v1/get-shop-orders?userId=${vendorId}&status_order=${statusOrder}&page=${page}&limit=${limit}`
        );
        const data = await response.json();
          console.log(data);
        if (data?.success) {
          const dataOrders = await this.convertDataOrders(data.orders);
          this.orders = dataOrders;
        } else {
          console.error("Failed to fetch orders:", data?.message);
        }
      } catch (error) {
        console.error("Error:", error);
        alert("Failed to fetch orders:", data?.message);
      }
    },

    //Get list order when role user = driver
    async getOrdersofDriver() {
      const driverPhone = this.driver?.phoneNo;

      //   try {
      //     const response = await fetch(
      //       `${host_node}/order/getByDriver/${driverPhone}`
      //     );
      //     const data = await response.json();

      //     if (data.success) {
      //       this.ordersDriver = data.orders.map((order) => ({
      //         id: order._id,
      //         cusName: order.cusName,
      //         cusPhone: order.cusPhone,
      //         driverPhone: order.driverPhone,
      //         locID: order.locID,
      //         shipAddr: order.shipAddr,
      //       }));
      //     } else {
      //       alert(data.message);
      //     }
      //   } catch (error) {
      //     alert("An error occurred");
      //     console.log(error);
      //   }
      // [032403Tree] get orders by driver phone
      try {
        const vendorId = 1010;
        let page = 1;
        const limit = 5;

        const response = await fetch(
          `${host_wp}/v1/get-shop-orders?userId=${vendorId}&driver_phone=${driverPhone}&page=${page}&limit=${limit}`
        );
        const data = await response.json();
        if (data?.success) {
          const dataOrdersDriver = await this.convertDataOrders(data.orders);

          this.ordersDriver = dataOrdersDriver;
        } else {
          console.error("Failed to fetch orders:", data?.message);
        }
      } catch (error) {
        alert("An error occurred");
        console.error("Error:", error);
      }
    },
    //Show icon driver
    showIconDriver: function (driver) {
      let el = document.createElement("div");
      el.className = driver.username + " marker2" + " drmr";
      el.value = driver.username;
      el.driverName = driver.username;
      var marker = new mapboxgl.Marker(el)
        .setLngLat(driver.locID)
        .setPopup(
          new mapboxgl.Popup({ offset: 25 }).setHTML(
            `<a class="w3-button" value="${driver.username}">${driver.username}<a><p>${driver.phoneNo}</p>`
          )
        )
        .addTo(this.map);
      this.markers.push(marker);
      marker.getElement().addEventListener("click", () => {
        this.toggleDriverColor(driver);
      });
    },
    //Show Icon order
    showIconOrder: function (order) {
      let el = document.createElement("div");
      el.className =
        "order_" +
        order.id +
        " driver_" +
        order.driverPhone +
        " marker" +
        " odmr";

      var marker = new mapboxgl.Marker(el)
        .setLngLat(order.locID)
        .setPopup(
          new mapboxgl.Popup({ offset: 25 }) // add popups
            .setHTML(
              `<h9>Phone: ${order.cusPhone}</h9><h9>Name: ${order.cusName}</h9><h9>Add: ${order.shipAddr}</h9>`
            )
        )
        .addTo(this.map);
      this.markers.push(marker);
    },

    //Select driver
    toggleDriverColor: async function (driver) {
      let anchorElement = document.querySelector(".driver_" + driver.username);

      let ordersSelected = this.orders.filter(
        (order) => order.driverPhone === driver.phoneNo
      );

      const isSelected = anchorElement.classList.contains("red-text");

      let currentAnchorDriverSelected = document.querySelector(
        ".dr" + ".red-text"
      );
      let currentAnchorOrderSelected = document.querySelectorAll(
        ".or" + ".red-text"
      );
      let currentDriverMarkerSelected = document.querySelector(
        ".drmr" + ".marker3"
      );
      let currentOrderMarkerSelected = document.querySelectorAll(
        ".odmr" + ".marker1"
      );

      if (!isSelected) {
        anchorElement.classList.add("red-text");
        if (currentAnchorDriverSelected) {
          currentAnchorDriverSelected.classList.remove("red-text");
        }
        if (currentAnchorOrderSelected.length !== 0) {
          currentAnchorOrderSelected.forEach((orderAnchor) => {
            orderAnchor.classList.remove("red-text");
          });
        }
        if (currentDriverMarkerSelected) {
          currentDriverMarkerSelected.classList.remove("marker3");
          currentDriverMarkerSelected.classList.add("marker2");
        }
        if (currentOrderMarkerSelected.length !== 0) {
          currentOrderMarkerSelected.forEach((orderMarker) => {
            orderMarker.classList.remove("marker1");
            orderMarker.classList.add("marker");
          });
        }
        const markerDriverSelected = document.querySelector(
          "." + driver.username + ".marker2"
        );
        if (markerDriverSelected) {
          markerDriverSelected.classList.remove("marker2");
          markerDriverSelected.classList.add("marker3");
        }
        ordersSelected.forEach((order) => {
          const selectedOrderMarker = document.querySelector(
            ".order_" + order.id + ".marker"
          );
          const addressOrderSelected = document.querySelector(
            ".address_" + order.id
          );
          if (selectedOrderMarker) {
            selectedOrderMarker.classList.remove("marker");
            selectedOrderMarker.classList.add("marker1");
          }

          if (addressOrderSelected) {
            addressOrderSelected.classList.add("red-text");
          }
        });
      } else {
        anchorElement.classList.remove("red-text");
        const markerDriverSelected = document.querySelector(
          "." + driver.username + ".marker3"
        );
        if (markerDriverSelected) {
          markerDriverSelected.classList.remove("marker3");
          markerDriverSelected.classList.add("marker2");
        }

        ordersSelected.forEach((order) => {
          const selectedOrderMarker = document.querySelector(
            ".order_" + order.id + ".marker1"
          );
          const addressOrderSelected = document.querySelector(
            ".address_" + order.id + ".red-text"
          );
          if (selectedOrderMarker) {
            selectedOrderMarker.classList.remove("marker1");
            selectedOrderMarker.classList.add("marker");
          }
          if (addressOrderSelected) {
            addressOrderSelected.classList.remove("red-text");
          }
        });
      }
    },

    //Hide/show icon driver
    toggleMarkerVisibility: function (driver) {
      const marker = document.querySelector("." + driver.username);

      const orderList = this.orders.filter(
        (order) => order.driverPhone === driver.phoneNo
      );

      if (driver.isHidden) {
        marker.style.display = "none";
        orderList.forEach((order) => {
          const selectedOrderMarker = document.querySelector(
            ".order_" + order.id
          );
          if (selectedOrderMarker) {
            selectedOrderMarker.style.display = "none";
          }
        });
      } else {
        marker.style.display = "block";
        orderList.forEach((order) => {
          const selectedOrderMarker = document.querySelector(
            ".order_" + order.id
          );
          if (selectedOrderMarker) {
            selectedOrderMarker.style.display = "block";
          }
        });
      }
    },

    // Show/hide icon order
    toggleMarkerOrderVisibility: function (order) {
      const marker = document.querySelector(".order_" + order.id);
      if (order.isHidden) {
        marker.style.display = "none";
      } else {
        marker.style.display = "block";
      }
    },

    // Show route driver to order
    checkRoute: async function (order) {
      var self = this;
      var driver = null;
      var driverId = null;

      if (this.role === "admin") {
        driver = this.drivers.find(
          (driver) => driver.phoneNo === order.driverPhone
        );
        driverId = driver.id;
      } else if (this.role === "driver") {
        driver = this.driver;
        driverId = driver._id;
      }

      var orderId = order?.id;
      try {
        const response = await fetch(`${host_node}/order/${driverId}`);
        const data = await response.json();

        if (data.success) {
          const driver = data.Driver;

          //   const order = data.orders.find((order) => order._id === orderId);

          const orderLocation = order?.locID;

          const driverLocation = driver?.locID;

          // Show new route
          const routeCheckbox = document.getElementById("route_" + orderId);

          if (routeCheckbox.checked) {
            if (!self.isDrawingRoute) {
              self.isDrawingRoute = true;
  
              setTimeout(async () => {
                // Update the route and marker position on the map
                const { totalDuration, routeDetails } = await this.drawRoute(
                  this.currentLocation,
                  [orderLocation]
                );
  
                this.socket.emit("routeUpdate", {
                  totalDuration: totalDuration,
                });
                console.log("totalDuration: " + totalDuration);
  
                // // Create a new <p> element to display location information
                // const locationInfo2 = document.createElement("p");
                // locationInfo2.textContent = `Duration: ${totalDuration}`;
  
                // // Append location information to the "result" element
                // duration.appendChild(locationInfo2);

                console.log(routeDetails);
            const driverMarker = document.querySelector(
              "." + driver.username + ".marker2"
            );
            const selectedOrderMarker = document.querySelector(
              ".order_" + order?.id + ".marker" // [032403Tree] change _id to id
            );

            if (driverMarker) {
              driverMarker.classList.remove("marker2");
              driverMarker.classList.add("marker3");
            }

            if (selectedOrderMarker) {
              selectedOrderMarker.classList.remove("marker");
              selectedOrderMarker.classList.add("marker1");
            }

            const instructionsDiv = document.getElementById("instructions");
            instructionsDiv.innerHTML = `<h4>Route Details</h4>`;
            instructionsDiv.innerHTML += `<li><b>Trip duration:</b> ${Math.ceil(
              totalDuration / 60
            )} min 🚴</li>`;

            let startTime = 0;
            let totalTime = 0;

            routeDetails.forEach((route, index) => {
              instructionsDiv.innerHTML += `<li><b>Start position | Time:</b> ${startTime} min 🚴 | <b>Duration:</b> ${startTime} min 🚴</li>`;
              startTime += Math.ceil(route.duration / 60);
              route.steps.forEach((step, stepIndex) => {
                instructionsDiv.innerHTML += `<li>${
                  step.maneuver.instruction
                } | Time: ${Math.ceil(
                  totalTime / 60
                )} min 🚴 | <b>Duration:</b> ${Math.ceil(
                  step.duration / 60
                )} min 🚴  </li>`;
                totalTime += step.duration;
              });
            });

            instructionsDiv.innerHTML += `<li><b>You have arrived at your 1st destination | Time:</b> ${startTime} min 🚴</li>`;
  
                self.isDrawingRoute = false;
              }, DRAW_ROUTE_DELAY);
            }
            
            
          } else {
            if (this.role === "admin") {
              let anchorElement = document.querySelector(
                ".driver_" + driver.username
              );

              const isSelected = anchorElement.classList.contains("red-text");

              if (isSelected) {
                if (this.map.getLayer("routeline-active"))
                  this.map.removeLayer("routeline-active");
                if (this.map.getLayer("routearrows"))
                  this.map.removeLayer("routearrows");
                if (this.map.getSource("route")) this.map.removeSource("route");
              } else {
                if (this.map.getLayer("routeline-active"))
                  this.map.removeLayer("routeline-active");
                if (this.map.getLayer("routearrows"))
                  this.map.removeLayer("routearrows");
                if (this.map.getSource("route")) this.map.removeSource("route");

                const selectedOrderMarker = document.querySelector(
                  ".order_" + order?.id + ".marker1" // // [032403Tree] change _id to id
                );

                if (selectedOrderMarker) {
                  selectedOrderMarker.classList.remove("marker1");
                  selectedOrderMarker.classList.add("marker");
                }

                const driverMarker = document.querySelector(
                  "." + driver.username + ".marker3"
                );

                if (driverMarker) {
                  driverMarker.classList.remove("marker3");
                  driverMarker.classList.add("marker2");
                }
              }
            } else if (this.role === "driver") {
              if (this.map.getLayer("routeline-active"))
                this.map.removeLayer("routeline-active");
              if (this.map.getLayer("routearrows"))
                this.map.removeLayer("routearrows");
              if (this.map.getSource("route")) this.map.removeSource("route");

              const selectedOrderMarker = document.querySelector(
                ".order_" + order?.id + ".marker1" // // [032403Tree] change _id to id
              );

              if (selectedOrderMarker) {
                selectedOrderMarker.classList.remove("marker1");
                selectedOrderMarker.classList.add("marker");
              }

              const driverMarker = document.querySelector(
                "." + driver.username + ".marker3"
              );

              if (driverMarker) {
                driverMarker.classList.remove("marker3");
                driverMarker.classList.add("marker2");
              }
            }

            // hide show routes
            const instructionsDiv = document.getElementById("instructions");
            instructionsDiv.innerHTML = `<h4>You don't have any routes yet</h4>`;
          }
        } else {
          alert(data.message);
        }
      } catch (error) {
        console.error("Error:", error);
        alert("An error occurred while fetching order data.");
      }
    },

    // Show route from driver to all orders
    checkRouteAll: async function () {
      var driver = this.driver;

      var driverId = driver._id;

      try {
        const response = await fetch(`${host_node}/order/${driverId}`);
        const data = await response.json();

        if (data.success) {
          const driver = data.Driver;

          const orders = data.orders;
          3;

          const driverLocation = driver.locID;

          const orderLocations = orders.map((order) => order.locID);

          // Caculate from driver location to all order location
          const distancesPromises = orderLocations.map((location) =>
            this.calculateDistance(driverLocation, location)
          );
          const distances = await Promise.all(distancesPromises);

          // Array order location and distance
          const ordersWithDistance = orderLocations.map((location, index) => {
            return {
              index: index,
              location: location,
              distance: distances[index],
            };
          });

          // sort distance
          ordersWithDistance.sort((a, b) => {
            return a.distance - b.distance;
          });

          //  List order location sorted
          const sortedOrderLocations = ordersWithDistance.map(
            (order) => order.location
          );

          console.log(sortedOrderLocations);

          // Show new route
          const routeCheckbox = document.getElementById("routeAllCheckbox");

          if (routeCheckbox.checked) {
            const { totalDuration, routeDetails } = await this.drawRoute(
              driverLocation,
              sortedOrderLocations
            );

            const driverMarker = document.querySelector(
              "." + driver.username + ".marker2"
            );

            orders.forEach((order) => {
              const selectedOrderMarker = document.querySelector(
                ".order_" + order._id + ".marker"
              );
              if (selectedOrderMarker) {
                selectedOrderMarker.classList.remove("marker");
                selectedOrderMarker.classList.add("marker1");
              }
            });

            if (driverMarker) {
              driverMarker.classList.remove("marker2");
              driverMarker.classList.add("marker3");
            }

            // Show instruction
            const instructionsDiv = document.getElementById("instructions");
            instructionsDiv.innerHTML = `<h4>Route Details</h4>`;
            instructionsDiv.innerHTML += `<li><b>Trip duration:</b> ${Math.ceil(
              totalDuration / 60
            )} min 🚴</li>`;

            let startTime = 0;
            let totalTime = 0;
            instructionsDiv.innerHTML += `<li><b>Start position | Time:</b> ${startTime} min 🚴 | <b>Duration:</b> ${startTime} min 🚴</li>`;
            routeDetails.forEach((route, index) => {
              startTime += Math.ceil(route.duration / 60);
              route.steps.forEach((step, stepIndex) => {
                instructionsDiv.innerHTML += `<li>${
                  step.maneuver.instruction
                } | Time: ${Math.ceil(
                  totalTime / 60
                )} min 🚴 | <b>Duration:</b> ${Math.ceil(
                  step.duration / 60
                )} min 🚴</li>`;
                totalTime += step.duration;
              });
            });

            instructionsDiv.innerHTML += `<li><b>You have arrived at your 1st destination | Time:</b> ${startTime} min 🚴</li>`;
          } else {
            if (this.map.getLayer("routeline-active"))
              this.map.removeLayer("routeline-active");
            if (this.map.getLayer("routearrows"))
              this.map.removeLayer("routearrows");
            if (this.map.getSource("route")) this.map.removeSource("route");

            orders.forEach((order) => {
              const selectedOrderMarker = document.querySelector(
                ".order_" + order._id + ".marker1"
              );
              if (selectedOrderMarker) {
                selectedOrderMarker.classList.remove("marker1");
                selectedOrderMarker.classList.add("marker");
              }
            });

            const driverMarker = document.querySelector(
              "." + driver.username + ".marker3"
            );

            if (driverMarker) {
              driverMarker.classList.remove("marker3");
              driverMarker.classList.add("marker2");
            }

            // Hide instructions
            const instructionsDiv = document.getElementById("instructions");
            instructionsDiv.innerHTML = `<h4>You don't have any routes yet</h4>`;
          }
        } else {
          alert(data.message);
        }
      } catch (error) {
        console.error("Error:", error);
        alert("An error occurred while fetching order data.");
      }
    },

    //Calculate distance route
    calculateDistance: async function (point1, point2) {
      const [lon1, lat1] = point1;
      const [lon2, lat2] = point2;

      const url = `https://api.mapbox.com/directions/v5/mapbox/driving/${lon1},${lat1};${lon2},${lat2}?access_token=${"pk.eyJ1IjoiYnJvbHk5OTk4IiwiYSI6ImNscnI5cG14cTAxZ28ydnBjcGx3a2xkaWEifQ.GJLQq76LqceD19Q6abBjrQ"}`;

      try {
        const response = await fetch(url);
        const data = await response.json();

        if (data.routes && data.routes.length > 0) {
          const distance = data.routes[0].distance;
          return distance;
        } else {
          console.error("No route found.");
          return null;
        }
      } catch (error) {
        console.error("Error:", error);
        return null;
      }
    },

    // Show route all map
    drawRoute: async function (driverLocation, destinations) {
      try {
        let totalDuration = 0;
        const routeDetails = [];
        const routeCoordinates = [];
        let previousPoint = driverLocation;

        // Remove existing layers and sources
        if (this.map.getLayer("routearrows"))
          this.map.removeLayer("routearrows");
        if (this.map.getLayer("routeline-active"))
          this.map.removeLayer("routeline-active");
        if (this.map.getLayer("dropoffs-symbol"))
          this.map.removeLayer("dropoffs-symbol");
        if (this.map.getSource("route")) this.map.removeSource("route");
        if (this.map.getSource("dropoffs-symbol"))
          this.map.removeSource("dropoffs-symbol");

        // Calculate routes
        for (const destination of destinations) {
          const response = await fetch(
            `https://api.mapbox.com/directions/v5/mapbox/driving/${previousPoint[0]},${previousPoint[1]};${destination[0]},${destination[1]}?geometries=geojson&steps=true&access_token=${mapboxgl.accessToken}`
          );
          const data = await response.json();
          const route = data.routes[0];

          totalDuration += route.duration;

          routeDetails.push({
            coordinates: route.geometry.coordinates,
            duration: route.duration,
            steps: route.legs[0].steps,
          });

          // Get location on routes
          const coordinates = data.routes[0].geometry.coordinates;
          routeCoordinates.push(...coordinates);

          previousPoint = destination;
        }

        // Display Route
        this.map.addSource("route", {
          type: "geojson",
          data: {
            type: "Feature",
            properties: {},
            geometry: {
              type: "LineString",
              coordinates: routeCoordinates,
            },
          },
        });

        this.map.addLayer({
          id: "routeline-active",
          type: "line",
          source: "route",
          layout: {
            "line-join": "round",
            "line-cap": "round",
          },
          paint: {
            "line-color": "#3887be",
            "line-width": ["interpolate", ["linear"], ["zoom"], 12, 3, 22, 12],
          },
        });

        this.map.addLayer({
          id: "routearrows",
          type: "symbol",
          source: "route",
          layout: {
            "symbol-placement": "line",
            "text-field": "▶",
            "text-size": ["interpolate", ["linear"], ["zoom"], 12, 24, 22, 60],
            "symbol-spacing": [
              "interpolate",
              ["linear"],
              ["zoom"],
              12,
              30,
              22,
              160,
            ],
            "text-keep-upright": false,
          },
          paint: {
            "text-color": "#3887be",
            "text-halo-color": "hsl(55, 11%, 96%)",
            "text-halo-width": 3,
          },
        });

        return { totalDuration, routeDetails };
      } catch (error) {
        console.error("Error:", error);
        alert("An error occurred while loading the route.");
        return false;
      }
    },

    openAssignPopup(order) {
      this.showAssignPopup = order?.id;
      this.selectedDriver = order?.driverName;
      console.log("driver selected: ", order?.driverName);
      console.log("driver selected: ", this.selectedDriver);
    },
    closeAssignPopup() {
      this.showAssignPopup = false;
    },
    async updateOrderPhoneNumber() {
      const selectedDriver = this.drivers.find(
        (driver) => driver.id == this.selectedDriver
      );

      if (selectedDriver) {
        const orderToUpdate = this.orders.find(
          (order) => order?.id == this.showAssignPopup
        );

        if (orderToUpdate) {
          orderToUpdate.driverPhone = selectedDriver?.phoneNo;

          const url = host_wp + "/v1/update-order-status";
          const fetchOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
              orderId: orderToUpdate?.id,
              driverPhone: selectedDriver?.phoneNo,
            }),
          };

          const res = await fetch(url, fetchOptions);
          const result = await res.json();
          console.log("result: " + result);
          if (result.success) {
            this.closeAssignPopup();
            alert("Assign order successfully");
          } else {
            alert("Assign order failed");
          }
        }
      }
    },
    async updateOrderByDriver(order) {
      try {
        const selectedStatus = this.selectedStatusOrder;
        const deliveryNotes = this.deliveryNotes;
        const url = host_wp + "/v1/update-order-status";
        const fetchOptions = {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            orderId: order?.id,
            orderStatus: selectedStatus,
            deliveryNote: deliveryNotes,
          }),
        };
        const res = await fetch(url, fetchOptions);
        const result = await res.json();
        // fake temporary
        if (result.success) {
          alert("Update order successfully");
        } else {
          alert("Update order failed");
        }
      } catch (error) {
        console.error("Error:", error);
      }
    },
    findMarkerByCoordinate(targetLngLat, self) {
      let targetMarkerIndex = self.markers.findIndex(function (marker) {
        var markerLngLat = marker?.getLngLat();
        return (
          markerLngLat?.lng == targetLngLat[0] &&
          markerLngLat?.lat == targetLngLat[1]
        );
      });

      return targetMarkerIndex;
    },
    async updateDriverLocation(driver) {
      console.log("driver addr fn1: " + this.driver?.locID);

      var self = this; // Lưu trữ ngữ cảnh của this
      var targetLngLat = this.driver?.locID;
      var targetMarkerIndex = this.findMarkerByCoordinate(targetLngLat, this);
      console.log("target index 1: ", targetMarkerIndex);

      try {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
            async function (position) {
              var latitude = position.coords.latitude;
              var longitude = position.coords.longitude;
              console.log("lat: " + latitude + ", lon: " + longitude);
              const response = await fetch(
                `${host_node}/driver/updateLocId/${driver?._id}`,
                {
                  method: "PUT",
                  headers: { "Content-Type": "application/json" },
                  body: JSON.stringify({
                    locID: [longitude, latitude],
                  }),
                }
              );
              const data = await response.json();

              console.log("data update location f1: " + JSON.stringify(data));
              if (data.success) {
                this.driver = data.driver;
                if (targetMarkerIndex !== -1) {
                  self.markers[targetMarkerIndex].remove();
                  self.markers.splice(targetMarkerIndex, 1);
                }
                // Di chuyển tới tọa độ mới
                self.map.flyTo({
                  center: [longitude, latitude],
                  zoom: 12.5,
                  speed: 1.2,
                  curve: 1.42,
                  easing: function (t) {
                    return t;
                  },
                });
                self.showIconDriver(this.driver);
                alert("Update location successfully");
              } else {
                alert("Failed to update your location");
              }
            },
            function (error) {
              alert(
                "Permission denied. Please clear location settings, then refresh page and try again."
              );
            }
          );
        }
      } catch (error) {
        console.error("Lỗi khi yêu cầu quyền truy cập vị trí:", error);
        alert(
          "Permission denied. Please open location settings and try again."
        );
      }
    },
    async updateDriverLocationByAddress(driver) {
      console.log(this.markers);
      const r = await fetch(`${host_node}/driver/getDriverById/${driver?._id}`);
      const da = await r.json();
      var self = this; // Lưu trữ ngữ cảnh của this
      console.log("driver addr fn2: " + da.Driver?.locID);
      var targetLngLat = da.Driver?.locID;
      var targetMarkerIndex = this.findMarkerByCoordinate(targetLngLat, this);
      console.log("target index 2: ", targetMarkerIndex);
      const response = await fetch(
        `${host_node}/driver/updateDriverAddr/${driver?._id}`,
        {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            currentAddress: this.currentAddress,
          }),
        }
      );
      const data = await response.json();

      if (data.success) {
        if (d.success) {
          if (targetMarkerIndex !== -1) {
            self.markers[targetMarkerIndex].remove();
            self.markers.splice(targetMarkerIndex, 1);
          }
          // Di chuyển tới tọa độ mới
          self.map.flyTo({
            center: this.driver?.locID,
            zoom: 12.5,
            speed: 1.2,
            curve: 1.42,
            easing: function (t) {
              return t;
            },
          });
          self.showIconDriver(this.driver);
          alert("Update location successfully");
        }
      } else {
        alert("Failed to update your location");
      }
    },
  },
  created() {
    // implement tracking here
  },
  mounted: async function () {
    await this.checkUserRole();
    await this.loadmap();
    await this.getOrders();
    await this.getDrivers();
    this.trackingLocation();
    if (this.role === "admin") {
      this.drivers.forEach((driver) => {
        this.showIconDriver(driver);
      });
      this.orders.forEach((order) => {
        this.showIconOrder(order);
      });
    } else if (this.role === "driver") {
      await this.getOrdersofDriver();
      this.showIconDriver(this.driver);
      this.ordersDriver.forEach((order) => {
        this.showIconOrder(order);
      });
    }

    // Get address when click on map
    this.map.on("click", async (e) => {
      var coordinates = e.lngLat;

      try {
        const response = await fetch(
          `https://api.mapbox.com/geocoding/v5/mapbox.places/${
            coordinates.lng
          },${
            coordinates.lat
          }.json?access_token=${"pk.eyJ1IjoiYnJvbHk5OTk4IiwiYSI6ImNscnI5cG14cTAxZ28ydnBjcGx3a2xkaWEifQ.GJLQq76LqceD19Q6abBjrQ"}`
        );
        const data = await response.json();

        console.log(data);

        const addressInfo = data.features[0];

        console.log("Address:", addressInfo.place_name);
      } catch (error) {
        console.error("Error fetching address information:", error);
      }
    });

    // const socket = new WebSocket(
    //   "wss://map-delivery-and-order-express.onrender.com:3000"
    // );

    // socket.addEventListener("open", function (event) {
    //   console.log("Connected to WebSocket server");
    // });

    // socket.addEventListener("message", function (event) {
    //   console.log("Message from server:", event.data);
    // });

    // socket.addEventListener("close", function (event) {
    //   console.log("Disconnected from WebSocket server");
    // });

    // socket.addEventListener("error", function (event) {
    //   console.error("WebSocket error:", event);
    // });
  },
});
